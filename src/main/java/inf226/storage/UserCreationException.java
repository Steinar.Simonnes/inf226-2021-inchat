package inf226.storage;

public class UserCreationException extends RuntimeException{
    public UserCreationException(String s){
        super(s);
    }
}
