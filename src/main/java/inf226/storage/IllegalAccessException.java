package inf226.storage;

public class IllegalAccessException extends RuntimeException {
    public IllegalAccessException(String s){
        super(s);
    }
    public IllegalAccessException(){
        super();
    }
}
