package inf226.storage;

public class UserExistsException extends UserCreationException{
    public UserExistsException(String s){
        super(s);
    }
}
