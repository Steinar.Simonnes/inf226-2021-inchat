package inf226.inchat;

import java.security.SecureRandom;
import java.util.Arrays;

public class Salt {
    private static final SecureRandom random = new SecureRandom();
    private static final int SALT_SIZE = 128;
    private final byte[] salt;
    public Salt(){
        salt = new byte[SALT_SIZE];
        random.nextBytes(salt);
    }

    public Salt(byte[] salt){
        this.salt = salt;
    }

    public byte[] toBytes(){
        return salt;
    }
    @Override
    public String toString(){return Arrays.toString(salt);}
}
