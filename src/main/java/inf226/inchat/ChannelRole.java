package inf226.inchat;

public enum ChannelRole {
    OWNER,
    MODERATOR,
    PARTICIPANT,
    OBSERVER,
    BANNED
}
