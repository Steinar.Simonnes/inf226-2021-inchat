package inf226.inchat;
import inf226.util.Triple;
import inf226.util.immutable.List;
import inf226.util.Pair;


import inf226.storage.*;

import java.awt.event.WindowStateListener;

/**
 * The Account class holds all information private to
 * a specific user.
 **/
public final class Account {
    /*
     * A channel consists of a User object of public account info,
     * and a list of channels which the user can post to.
     */
    public final Stored<User> user;
    public final List<Triple<String, Stored<Channel>, ChannelRole>> channels;
    public final Password password;
    
    public Account(final Stored<User> user, final List<Triple<String,Stored<Channel>,ChannelRole>> channels, final String attemptedpassword) throws ShittyPasswordException {
        this.user = user;
        this.channels = channels;
        this.password = Password.create(attemptedpassword);
    }

    public Account(final Stored<User> user, final List<Triple<String,Stored<Channel>,ChannelRole>> channels, final Password password){
        this.user = user;
        this.channels = channels;
        this.password = password;
    }

    /**
     * Create a new Account.
     *
     * @param user The public User profile for this user.
     * @param password The login password for this account.
     **/
    public static Account create(final Stored<User> user, final String password){
        return new Account(user, List.empty(), password);
    }

    @Override
    public boolean equals(Object other){
        if ( ! (other instanceof Account)) return false;
        Account o = (Account) other;
        return o.user.equals(this.user) && o.channels.equals(this.channels);
        //Sjekker ikke om passordet er det samme, siden brukeren i grunnen er en PK
    }
    
    /**
     * Join a channel with this account.
     *
     * @return A new account object with the channel added.
     */
    public Account joinChannel(final String alias, final Stored<Channel> channel,ChannelRole role) {
        Triple<String,Stored<Channel>,ChannelRole> entry = new Triple<String,Stored<Channel>,ChannelRole>(alias,channel,role);
        return new Account(user, List.cons(entry, channels), password);
    }


    /**
     * Check weather if a string is a correct password for
     * this account.
     *
     * @return true if password matches.
     */
    public boolean checkPassword(String password) {
        return this.password.check(password);
    }
}
