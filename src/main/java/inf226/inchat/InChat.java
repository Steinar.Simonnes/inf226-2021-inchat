package inf226.inchat;

import inf226.storage.*;
import inf226.storage.IllegalAccessException;
import inf226.util.Maybe;
import inf226.util.Pair;
import inf226.util.Triple;
import inf226.util.Util;

import java.util.TreeMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.UUID;
import java.time.Instant;
import java.sql.SQLException;
import java.sql.Connection;


import inf226.util.immutable.List;

/**
 * This class models the chat logic.
 *
 * It provides an abstract interface to
 * usual chat server actions.
 *
 **/

public class InChat {
    private final Connection connection;
    private final UserStorage userStore;
    private final ChannelStorage channelStore;
    private final AccountStorage accountStore;
    private final SessionStorage sessionStore;
    private final Map<UUID,List<Consumer<Channel.Event>>> eventCallbacks = new TreeMap<UUID,List<Consumer<Channel.Event>>>();

    public InChat(UserStorage userStore, ChannelStorage channelStore, AccountStorage accountStore, SessionStorage sessionStore, Connection connection) {
        this.userStore=userStore;
        this.channelStore=channelStore;
        this.accountStore=accountStore;
        this.sessionStore=sessionStore;
        this.connection=connection;
    }

    public Maybe<ChannelRole> getRole(Stored<Account> account, Stored<Channel> channel) throws SQLException{
        return accountStore.getRole(account, channel);
    }

    /**
     * An atomic operation in Inchat.
     * An operation has a function run(), which returns its
     * result through a consumer.
     */
    @FunctionalInterface
    private interface Operation<T,E extends Throwable> {
        void run(final Consumer<T> result) throws E, DeletedException, Maybe.NothingException, UpdatedException;
    }
    /**
     * Execute an operation atomically in SQL.
     * Wrapper method for commit() and rollback().
     */
    private<T> Maybe<T> atomic(Operation<T,SQLException> op) {
        synchronized (connection) {
            try {
                Maybe.Builder<T> result = Maybe.builder();
                op.run(result);
                connection.commit();
                return result.getMaybe();
            }catch (SQLException | DeletedException | UserCreationException | Maybe.NothingException | UpdatedException | IllegalAccessException e) {
                synchronized (System.err){
                    System.err.println(e.toString());
                }
            }

            try {
                connection.rollback();
            } catch (SQLException e) {
                System.err.println(e.toString());
            }
            return Maybe.nothing();
        }
    }

    /**
     * Log in a user to the chat.
     */
    public Maybe<Stored<Session>> login(final String username, final String password) {
        return atomic(result -> {
            final Stored<Account> account = accountStore.lookup(username);
            final Stored<Session> session = sessionStore.save(new Session(account, Instant.now().plusSeconds(60*60*24)));
            if (account.value.checkPassword(password)) {
                result.accept(session);
            }
        });
    }
    
    /**
     * Register a new user.
     */
    public Maybe<Stored<Session>> register(final String attemptedusername,final String attemptedpassword) {
        return atomic(result -> {
            if (userStore.userNameExists(attemptedusername)) throw new UserExistsException("The user already exists!");
            final Stored<User> user = userStore.save(User.create(attemptedusername));
            final Stored<Account> account = accountStore.save(Account.create(user, attemptedpassword));
            final Stored<Session> session = sessionStore.save(new Session(account, Instant.now().plusSeconds(60*60*24)));
            result.accept(session); 
        });
    }
    
    /**
     * Restore a previous session.
     */
    public Maybe<Stored<Session>> restoreSession(UUID sessionId) {
        return atomic(result -> result.accept(sessionStore.get(sessionId)));
    }
    
    /**
     * Log out and invalidate the session.
     */
    public void logout(Stored<Session> session) {
        atomic(result -> Util.deleteSingle(session,sessionStore));
    }


    public Maybe<Stored<Channel>> updateRole(Stored<Account> account, Stored<Channel> channel, String targetedUsername, String newrole) {
        return atomic(result -> {
            ChannelRole subjectRole = Util.lookupPair(account.value.channels, channel.value.name).get().second;
            if ( ! subjectRole.equals(ChannelRole.OWNER)) throw new IllegalAccessException("Only owners may set other users' roles");
            if (account.value.user.value.name.toString().equals(targetedUsername)) throw new IllegalAccessException("May not edit your own role");
            Stored<Account> target = accountStore.lookup(targetedUsername);
            List<Triple<String, Stored<Channel>, ChannelRole>> newChannels = Util.replaceC(target.value.channels, channel, ChannelRole.valueOf(newrole));

            if (newChannels.equals(target.value.channels)){
                newChannels = newChannels.add(new Triple<String,Stored<Channel>,ChannelRole>(channel.value.name, channel, ChannelRole.valueOf(newrole)));
            }
            accountStore.update(target, new Account(target.value.user, newChannels, target.value.password));
            System.err.println(target.value.user.value.name + " is now a " + newrole);

            // Om eieren vil overrekke eierskapet
            // Vi vil at enhver kanal alltid har nøyaktig én eier.
            if(newrole.equals("OWNER")){
                List<Triple<String,Stored<Channel>,ChannelRole>> channels = Util.replaceC(account.value.channels, channel, ChannelRole.MODERATOR);
                accountStore.update(account, new Account(target.value.user, channels, target.value.password));
                System.err.println(account.value.user.value.name.getName() + " has transferred their ownership and is now a moderator");
            }

            result.accept(channel);
        });
    }
    
    /**
     * Create a new channel.
     */
    public Maybe<Stored<Channel>> createChannel(Stored<Account> account, String name) {
        return atomic(result -> {
            Stored<Channel> channel = channelStore.save(new Channel(name,List.empty()));
            joinChannel(account, channel.identity, ChannelRole.OWNER);
            result.accept(channel);
        });
    }
    
    /**
     * Join a channel.
     */
    public Maybe<Stored<Channel>> joinChannel(Stored<Account> account, UUID channelID, ChannelRole role) {
        return atomic(result -> {
            Stored<Channel> channel = channelStore.get(channelID);
            try{
                ChannelRole actualRole = Util.lookupPair(account.value.channels, channel.value.name).get().second;
                if (actualRole.equals(ChannelRole.BANNED)) throw new IllegalAccessException("User is banned from this channel");
            }catch (Maybe.NothingException e){
                // Dette skjer om brukeren ikke allerede er i kanalen, og det er helt greit.
                // Vi gjør denne sjekken kun for å se om brukeren er bannlyst eller ikke.
            }
            Util.updateSingle(account, accountStore, a -> a.value.joinChannel(channel.value.name,channel,role));
            Stored<Channel.Event> joinEvent = channelStore.eventStore.save(Channel.Event.createJoinEvent(channelID, Instant.now(), account.value.user.value.name));
            result.accept(Util.updateSingle(channel, channelStore, c -> c.value.postEvent(joinEvent)));
        });
    }
    
    /**
     * Post a message to a channel.
     */
    public Maybe<Stored<Channel>> postMessage(Stored<Account> account, Stored<Channel> channel, String message) {
        return atomic(result -> {
            ChannelRole role = Util.lookupPair(account.value.channels, channel.value.name).get().second;
            if (role.equals(ChannelRole.BANNED) || role.equals(ChannelRole.OBSERVER)) throw new IllegalAccessException("User may not post here");
            Stored<Channel.Event> event = channelStore.eventStore.save(Channel.Event.createMessageEvent(channel.identity,Instant.now(), account.value.user.value.name, message));
            result.accept(Util.updateSingle(channel, channelStore, c -> c.value.postEvent(event)));
        });
    }
    
    /**
     * A blocking call which returns the next state of the channel.
     */
    public Maybe<Stored<Channel>> waitNextChannelVersion(UUID identity, UUID version) {
        try{
            return Maybe.just(channelStore.waitNextVersion(identity, version));
        } catch (DeletedException | SQLException e) {
            return Maybe.nothing();
        }
    }
    
    /**
     * Get an event by its identity.
     */
    public Maybe<Stored<Channel.Event>> getEvent(UUID eventID) {
        return atomic(result -> result.accept(channelStore.eventStore.get(eventID)));
    }
    
    /**
     * Delete an event.
     */
    public Stored<Channel> deleteEvent(Stored<Account> account, Stored<Channel> channel, Stored<Channel.Event> event) {
        return this.<Stored<Channel>>atomic(result -> {
            ChannelRole role = Util.lookupPair(account.value.channels, channel.value.name).get().second;
            if ( ! role.equals(ChannelRole.OWNER) && ! role.equals(ChannelRole.MODERATOR) && ! event.value.sender.getName().equals(account.value.user.value.name.getName())){
                throw new IllegalAccessException("Only the owner and its moderators may delete other people's messages");
            }
            Util.deleteSingle(event , channelStore.eventStore);
            result.accept(channelStore.noChangeUpdate(channel.identity));
        }).defaultValue(channel);
    }

    /**
     * Edit a message.
     */
    public Stored<Channel> editMessage(Stored<Account> account, Stored<Channel> channel, Stored<Channel.Event> event, String newMessage) {
        return this.<Stored<Channel>>atomic(result -> {
            ChannelRole role = Util.lookupPair(account.value.channels, channel.value.name).get().second;
            if ( ! role.equals(ChannelRole.OWNER) && ! role.equals(ChannelRole.MODERATOR) && !  event.value.sender.getName().equals(account.value.user.value.name.getName())){
                throw new IllegalAccessException("Only the owner and their moderators may edit other people's messages, \nand" +
                        account.value.user.value.name.getName() + " is only a " + role.toString() + ".");
            }
            Util.updateSingle(event, channelStore.eventStore, e -> e.value.setMessage(newMessage));
            result.accept(channelStore.noChangeUpdate(channel.identity));
        }).defaultValue(channel);
    }
}


