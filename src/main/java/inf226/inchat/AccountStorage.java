package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.UUID;

import inf226.storage.*;

import inf226.util.immutable.List;
import inf226.util.*;

/**
 * This class stores accounts in the database.
 */
public final class AccountStorage implements Storage<Account,SQLException> {
    
    final Connection connection;
    final Storage<User,SQLException> userStore;
    final Storage<Channel,SQLException> channelStore;
   
    /**
     * Create a new account storage.
     *
     * @param  connection   The connection to the SQL database.
     * @param  userStore    The storage for User data.
     * @param  channelStore The storage for channels.
     */
    public AccountStorage(Connection connection, Storage<User,SQLException> userStore, Storage<Channel,SQLException> channelStore) throws SQLException {
        this.connection = connection;
        this.userStore = userStore;
        this.channelStore = channelStore;
        
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Account (id TEXT PRIMARY KEY, version TEXT, user TEXT, password TEXT, salt TEXT, FOREIGN KEY(user) REFERENCES User(id) ON DELETE CASCADE)");
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS AccountChannel (account TEXT, channel TEXT, role TEXT, alias TEXT, ordinal INTEGER, PRIMARY KEY(account,channel), FOREIGN KEY(account) REFERENCES Account(id) ON DELETE CASCADE, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Account> save(Account account) throws SQLException {
        final Stored<Account> stored = new Stored<Account>(account);
        String sql = "INSERT INTO ACCOUNT VALUES(?, ?, ?, ?, ?)";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, stored.identity.toString());
        stmt.setString(2, stored.version.toString());
        stmt.setString(3, account.user.identity.toString());
        stmt.setString(4, account.password.toString());
        stmt.setString(5, account.password.getSalt().toString());
        stmt.execute();

        // Write the list of channels
        final Maybe.Builder<SQLException> exception = Maybe.builder();
        final Mutable<Integer> ordinal = new Mutable<Integer>(0);
        account.channels.forEach(element -> {
            try {
                String sql2 = "INSERT INTO AccountChannel VALUES(?, ?, ?, ?, ?)";
                PreparedStatement stmt2 = connection.prepareStatement(sql2);
                stmt2.setString(1, stored.identity.toString());
                stmt2.setString(2, element.second.identity.toString());
                stmt2.setString(3, element.third.toString());
                stmt2.setString(4, element.first);
                stmt2.setString(5, ordinal.get().toString());
                //connection.createStatement().executeUpdate(msql);
                stmt2.execute();
            }
            catch (SQLException e) { exception.accept(e) ; }
            ordinal.accept(ordinal.get() + 1);
        });

        Util.throwMaybe(exception.getMaybe());
        return stored;
    }
    
    @Override
    public synchronized Stored<Account> update(Stored<Account> account, Account new_account) throws UpdatedException, DeletedException, SQLException {
        final Stored<Account> current = get(account.identity);
        final Stored<Account> updated = current.newVersion(new_account);
        if(current.version.equals(account.version)) {
            String sql = "UPDATE Account SET (version, user) = (?, ?) WHERE id = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, updated.version.toString());
            stmt.setString(2, new_account.user.identity.toString());
            stmt.setString(3, updated.identity.toString());
            stmt.execute();
            connection.createStatement().executeUpdate("DELETE FROM AccountChannel WHERE account='" + account.identity + "'");

            final Maybe.Builder<SQLException> exception = Maybe.builder();
            final Mutable<Integer> ordinal = new Mutable<Integer>(0);
            new_account.channels.forEach(element -> {
                try {
                    String msql = "INSERT INTO AccountChannel VALUES(?, ?, ?, ?, ?)";
                    PreparedStatement stmt2 = connection.prepareStatement(msql);
                    stmt2.setString(1, account.identity.toString());
                    stmt2.setString(2, element.second.identity.toString());
                    stmt2.setString(3, element.third.toString());
                    stmt2.setString(4, element.first);
                    stmt2.setString(5, ordinal.get().toString());
                    stmt2.execute();
                }
                catch (SQLException e) { exception.accept(e) ; }
                ordinal.accept(ordinal.get() + 1);
            });

            Util.throwMaybe(exception.getMaybe());
        } else {
            throw new UpdatedException(current);
        }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Account> account) throws UpdatedException, DeletedException, SQLException {
            final Stored<Account> current = get(account.identity);
            if(current.version.equals(account.version)) {
                //String sql =  "DELETE FROM Account WHERE id ='" + account.identity + "'";
                //connection.createStatement().executeUpdate(sql);
                String sql = "DELETE FROM Account WHERE id = ?";
                PreparedStatement stmt = connection.prepareStatement(sql);
                stmt.setString(1, account.identity.toString());
                stmt.execute();
            } else {
            throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Account> get(UUID id) throws DeletedException, SQLException {

        //final String accountsql = "SELECT version,user,password,salt FROM Account WHERE id = '" + id.toString() + "'";
        //final String channelsql = "SELECT channel,alias,ordinal FROM AccountChannel WHERE account = '" + id.toString() + "' ORDER BY ordinal DESC";

        String accountsql = "SELECT version,user,password,salt FROM Account WHERE id = ?";
        String channelsql = "SELECT channel,alias,role,ordinal FROM AccountChannel WHERE account = ? ORDER BY ordinal DESC";

        final PreparedStatement accountStatement = connection.prepareStatement(accountsql);
        final PreparedStatement channelStatement = connection.prepareStatement(channelsql);

        accountStatement.setString(1, id.toString());
        channelStatement.setString(1, id.toString());

        final ResultSet accountResult = accountStatement.executeQuery();
        final ResultSet channelResult = channelStatement.executeQuery();

        if(accountResult.next()) {
            final UUID version = UUID.fromString(accountResult.getString("version"));
            final UUID userid  = UUID.fromString(accountResult.getString("user"));
            final String pswrd = accountResult.getString("password");
            final String salt  = accountResult.getString("salt");

            final Password password = Password.constructFromDatabase(pswrd, salt);
            final Stored<User> user = userStore.get(userid);
            // Get all the channels associated with this account
            final List.Builder<Triple<String,Stored<Channel>,ChannelRole>> channels = List.builder();
            while(channelResult.next()) {
                final UUID channelId = UUID.fromString(channelResult.getString("channel"));
                final String alias = channelResult.getString("alias");
                final String role  = channelResult.getString("role");
                channels.accept(new Triple<String,Stored<Channel>,ChannelRole>(alias,channelStore.get(channelId),ChannelRole.valueOf(role)));
            }
            return (new Stored<>(new Account(user,channels.getList(),password),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up an account based on their username.
     */
    public Stored<Account> lookup(String username) throws DeletedException, SQLException {
        //final String sql = "SELECT Account.id from Account INNER JOIN User ON user=User.id where User.name='" + username + "'";
        String sql =         "SELECT Account.id FROM Account INNER JOIN User ON user = User.id WHERE User.name = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, username);
        ResultSet rs =  stmt.executeQuery();

        //final Statement statement = connection.createStatement();
        //final ResultSet rs = statement.executeQuery(sql);
        if(rs.next()) {
            final UUID identity = UUID.fromString(rs.getString("id"));
            return get(identity);
        }
        throw new DeletedException();
    }

    public Maybe<ChannelRole> getRole(Stored<Account> account, Stored<Channel> channel) throws SQLException{
        final String sql = "SELECT role FROM AccountChannel WHERE account = ? AND channel = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, account.identity.toString());
        stmt.setString(2, channel.identity.toString());
        ResultSet rs = stmt.executeQuery();
        if (rs.next()){
            return new Maybe<>(ChannelRole.valueOf(rs.getString(1)));
        }return Maybe.nothing();
    }
}