package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;

import inf226.storage.*;
import inf226.util.*;
import inf226.util.immutable.List;


public final class EventStorage implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) throws SQLException {
        this.connection = connection;
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, channel TEXT, type INTEGER, time TEXT, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event) throws SQLException {
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);
        /*String sql =  "INSERT INTO Event VALUES('" + stored.identity + "','"
                                                  + stored.version  + "','"
                                                  + event.channel + "','"
                                                  + event.type.code + "','"
                                                  + event.time  + "')";
        connection.createStatement().executeUpdate(sql);*/
        String sql = "INSERT INTO Event VALUES(?, ?, ?, ?, ?)";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, stored.identity.toString());
        stmt.setString(2, stored.version.toString());
        stmt.setString(3, event.channel.toString());
        stmt.setString(4, event.type.code.toString());
        stmt.setString(5, event.time.toString());
        stmt.execute();
        switch (event.type) {
            case message:
                /*sql = "INSERT INTO Message VALUES('" + stored.identity + "','"
                                                     + event.sender + "','"
                                                     + event.message +"')";*/
                sql = "INSERT INTO Message VALUES(?, ?, ?)";
                stmt = connection.prepareStatement(sql);
                stmt.setString(1, stored.identity.toString());
                stmt.setString(2, event.sender.getName());
                stmt.setString(3, event.message);
                break;
            case join:
                /*sql = "INSERT INTO Joined VALUES('" + stored.identity + "','"
                                                  + event.sender +"')";*/
                sql = "INSERT INTO Joined Values(?, ?)";
                stmt = connection.prepareStatement(sql);
                stmt.setString(1, stored.identity.toString());
                stmt.setString(2, event.sender.getName());
                break;
        }
        //connection.createStatement().executeUpdate(sql);
        stmt.execute();
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event, Channel.Event new_event) throws UpdatedException, DeletedException, SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        /*String sql = "UPDATE Event SET" +
            " (version,channel,time,type) =('" 
                            + updated.version  + "','"
                            + new_event.channel  + "','"
                            + new_event.time  + "','"
                            + new_event.type.code
                            + "') WHERE id='"+ updated.identity + "'";
        connection.createStatement().executeUpdate(sql);*/
        String sql = "UPDATE Event SET (version, channel, time, type) = (?, ?, ?, ?) WHERE id = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, updated.version.toString());
        stmt.setString(2, new_event.channel.toString());
        stmt.setString(3, new_event.time.toString());
        stmt.setString(4, new_event.type.code.toString());
        stmt.setString(5, updated.identity.toString());
        stmt.execute();
        switch (new_event.type) {
            case message:
                /*sql = "UPDATE Message SET (sender,content)=('" + new_event.sender + "','"
                                                     + new_event.message +"') WHERE id='"+ updated.identity + "'";*/
                sql = "UPDATE Message SET (sender, content) = (?, ?) WHERE id = ?";
                stmt = connection.prepareStatement(sql);
                stmt.setString(1, new_event.sender.getName());
                stmt.setString(2, new_event.message);
                stmt.setString(3, updated.identity.toString());
                break;
            case join:
                /*sql = "UPDATE Joined SET (sender)=('" + new_event.sender +"') WHERE id='"+ updated.identity + "'";*/
                sql = "UPDATE Joined SET (sender) = (?) WHERE id = ?";
                stmt = connection.prepareStatement(sql);
                stmt.setString(1, new_event.sender.getName());
                stmt.setString(2, updated.identity.toString());
                break;
        }
        //connection.createStatement().executeUpdate(sql);
        stmt.execute();
    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event) throws UpdatedException, DeletedException, SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
            /*String sql =  "DELETE FROM Event WHERE id ='" + event.identity + "'";
            connection.createStatement().executeUpdate(sql);*/
            String sql = "DELETE FROM Event WHERE id = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, event.identity.toString());
            stmt.execute();
        } else {
            throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id) throws DeletedException, SQLException {
        /*final String sql = "SELECT version,channel,time,type FROM Event WHERE id = '" + id.toString() + "'";
        final Statement statement = connection.createStatement();
        final ResultSet rs = statement.executeQuery(sql);*/
        String sql = "SELECT version, channel, time, type FROM Event WHERE id = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, id.toString());
        ResultSet rs = stmt.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final UUID channel = UUID.fromString(rs.getString("channel"));
            final Channel.Event.Type type = Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = Instant.parse(rs.getString("time"));

            switch(type) {
                case message:
                    /*final String msql = "SELECT sender,content FROM Message WHERE id = '" + id.toString() + "'";
                    final ResultSet mrs = mstatement.executeQuery(msql);*/
                    String msql = "SELECT sender, content FROM Message WHERE id = ?";
                    PreparedStatement mstmt = connection.prepareStatement(msql);
                    mstmt.setString(1, id.toString());
                    ResultSet mrs = mstmt.executeQuery();
                    mrs.next();
                    UserName user = new UserName(mrs.getString("sender"));
                    return new Stored<Channel.Event>(Channel.Event.createMessageEvent(channel,time,user,mrs.getString("content")), id, version);
                case join:
                    /*final String asql = "SELECT sender FROM Joined WHERE id = '" + id.toString() + "'";
                    final ResultSet ars = mstatement.executeQuery(asql);*/
                    String asql = "SELECT sender from Joined WHERE id = ?";
                    PreparedStatement astmt = connection.prepareStatement(asql);
                    astmt.setString(1, id.toString());
                    ResultSet ars = astmt.executeQuery();
                    ars.next();
                    UserName user2 = new UserName(ars.getString("sender"));
                    return new Stored<Channel.Event>(Channel.Event.createJoinEvent(channel,time,user2));
            }
        }
        throw new DeletedException();
    }
}


 
