package inf226.inchat;

import inf226.storage.ShittyNameException;

public class UserName {
    private final String name;

    public UserName(String name) throws ShittyNameException {
        if(name.length() < 1) throw new ShittyNameException("The name is too short!");
        if(name.length() > 25) throw new ShittyNameException("Name is to long!");
        if(name.contains("?")) throw new ShittyNameException("Illegal character!");
        this.name = name;
    }
    public String getName(){
        return name;
    }
    @Override
    public String toString(){ return name; }
}
