package inf226.inchat;

import com.lambdaworks.crypto.SCrypt;
import inf226.storage.ShittyPasswordException;

import java.security.GeneralSecurityException;
import java.util.Arrays;

public class Password {
    private final byte[] password;
    private final Salt salt;

    private Password(String password){
        this.salt = new Salt();
        this.password = encrypt(password.getBytes(), salt.toBytes());
    }

    private Password(byte[] pswrd, Salt salt){
        this.password = pswrd;
        this.salt = salt;
    }

    /**
     * Gitt to strenger lest fra en database, konstruér et Password.
     *
     * Begge strengene må være strengrepresentasjoner av byte[],
     * f. eks. som "[1, 2, 3, 4]"
     *
     * @param passwordArray strengrepresentasjon av en byte[], som vil si hashen til passordet
     * @param saltArray strengrepresentasajon av en byte[], som vil si saltet til passordet
     * @return Password
     */
    public static Password constructFromDatabase(String passwordArray, String saltArray){
        return new Password(parse(passwordArray), new Salt(parse(saltArray)));
    }

    /**
     * Lager et nytt passord, genererer noe salt, og lagrer informasjonen som en byte[].
     *
     * Det eneste kravet er at lengden må være >= 8.
     *
     * @param password Hva passordet skal være
     * @return new Password
     * @throws ShittyPasswordException Om passordet er, vel, shitty.
     */
    public static Password create(String password) throws ShittyPasswordException{
        if (password.length() < 8) throw new ShittyPasswordException("Password is too short!");

        //Kan legge til flere sjekker her om vi vil.
        //For eksempel kan vi finne på en måte å ikke tillate '12345678', 'abcdefgh', eller 'password'.

        return new Password(password);
    }

    public boolean check(String attempt){
        return Arrays.equals(password, encrypt(attempt.getBytes(), salt.toBytes()));
    }

    public byte[] hash(){
        return password;
    }

    public Salt getSalt(){
        return salt;
    }

    private static byte[] parse(String str) throws NumberFormatException{
        str = str.replace("[", "");
        str = str.replace("]", "");
        String[] vals = str.split(",");
        byte[] ret = new byte[vals.length];
        for (int i = 0; i < vals.length; i++) {
            ret[i] = Byte.parseByte(vals[i].trim());
        }
        return ret;
    }

    private static byte[] encrypt(byte[] password, byte[] salt){
        try {
            return SCrypt.scrypt(password, salt, 256, 8, 8, 128);
        }catch (GeneralSecurityException ex){
            // TODO: 26.10.2021 Utbedre dette
            System.err.println("Failed to encrypt a password. Uh-oh, we literally have no idea how to handle this, so here's a null pointer.");
            ex.printStackTrace();
            return null; //Håper dette aldri skjer
        }
    }

    @Override
    public boolean equals(Object other) throws UnsupportedOperationException{
        throw new UnsupportedOperationException("Please stop using this method, use .check() instead");
    }

    @Override
    public String toString() throws UnsupportedOperationException{
        return Arrays.toString(password);
        //throw new UnsupportedOperationException("Absolutely do not use this. Please.");
    }
}
