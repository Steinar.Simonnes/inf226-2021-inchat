package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;
import inf226.util.*;



/**
 * The UserStore stores User objects in a SQL database.
 */
public final class UserStorage implements Storage<User,SQLException> {
    
    final Connection connection;
    
    public UserStorage(Connection connection) throws SQLException {
        this.connection = connection;
        connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS User (id TEXT PRIMARY KEY, version TEXT, name TEXT, joined TEXT)");
    }
    
    @Override
    public Stored<User> save(User user) throws SQLException {
        final Stored<User> stored = new Stored<User>(user);
        /*String sql =  "INSERT INTO User VALUES('" + stored.identity + "','"
                                                  + stored.version  + "','"
                                                  + user.name  + "','"
                                                  + user.joined.toString() + "')";
        connection.createStatement().executeUpdate(sql);*/
        String sql = "INSERT INTO User VALUES(?, ?, ?, ?)";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, stored.identity.toString());
        stmt.setString(2, stored.version.toString());
        stmt.setString(3, user.name.toString());
        stmt.setString(4, user.joined.toString());
        stmt.execute();
        return stored;
    }
    
    @Override
    public synchronized Stored<User> update(Stored<User> user, User new_user) throws UpdatedException, DeletedException, SQLException {
        final Stored<User> current = get(user.identity);
        final Stored<User> updated = current.newVersion(new_user);
        if(current.version.equals(user.version)) {
            /*String sql = "UPDATE User SET" +
            " (version,name,joined) =('" 
                            + updated.version  + "','"
                            + new_user.name  + "','"
                            + new_user.joined.toString()
                            + "') WHERE id='"+ updated.identity + "'";
            connection.createStatement().executeUpdate(sql);*/
            String sql = "UPDATE User SET (version, name, joined) = (?, ?, ?) WHERE id = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, updated.version.toString());
            stmt.setString(2, new_user.name.toString());
            stmt.setString(3, new_user.joined.toString());
            stmt.setString(4, updated.identity.toString());
            stmt.execute();
        } else {
            throw new UpdatedException(current);
        }
        return updated;
    }

    @Override
    public synchronized void delete(Stored<User> user) throws UpdatedException, DeletedException, SQLException {
        final Stored<User> current = get(user.identity);
        if(current.version.equals(user.version)) {
            /*String sql =  "DELETE FROM User WHERE id ='" + user.identity + "'";
            connection.createStatement().executeUpdate(sql);*/
            String sql = "DELETE FROM User WHERE id = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, user.identity.toString());
            stmt.execute();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<User> get(UUID id) throws DeletedException, SQLException {
        /*final String sql = "SELECT version,name,joined FROM User WHERE id = '" + id.toString() + "'";
        final Statement statement = connection.createStatement();
        final ResultSet rs = statement.executeQuery(sql);*/
        String sql = "SELECT version,name,joined FROM User WHERE id = ?";
        PreparedStatement stmt = connection.prepareStatement(sql);
        stmt.setString(1, id.toString());
        ResultSet rs = stmt.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final String name = rs.getString("name");
            final Instant joined = Instant.parse(rs.getString("joined"));
            return (new Stored<User> (new User(new UserName(name),joined),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up a user by their username;
     **/
    public Maybe<Stored<User>> lookup(String name) {
        try{
            /*final String sql = "SELECT id FROM User WHERE name = '" + name + "'";
            final Statement statement = connection.createStatement();
            final ResultSet rs = statement.executeQuery(sql);*/
            String sql = "SELECT id FROM User WHERE name = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            if(rs.next())
                return Maybe.just(get(UUID.fromString(rs.getString("id"))));
        } catch (Exception e) {

        }
        return Maybe.nothing();
    }

    public boolean userNameExists(String name){
        try{
            /*final String sql = "SELECT Count(*) FROM User WHERE Name='" + name + "'";
            final Statement stmnt = connection.createStatement();
            final ResultSet rs = stmnt.executeQuery(sql);*/
            String sql = "SELECT Count(*) FROM User WHERE name = ?";
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            return Integer.parseInt(rs.getString(1)) > 0;
        }catch (SQLException e){
            e.printStackTrace();
            return true;
        }
    }
}


