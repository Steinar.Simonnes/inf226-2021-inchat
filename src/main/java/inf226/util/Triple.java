package inf226.util;

public class Triple<A, B, C> {
    public final A first;
    public final B second;
    public final C third;
    public Triple(A a, B b, C c){
        first  = a;
        second = b;
        third  = c;
    }

    @Override
    public String toString(){
        return "(" + first + ", " + second + ", " + third + ")";
    }
}
