package inf226.util;
import inf226.util.immutable.List;
import java.lang.Throwable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;

import inf226.storage.*;


public class Util {
   public static<E extends Throwable> void throwMaybe(Maybe<E> exception) throws E {
       try { throw exception.get(); }
       catch (Maybe.NothingException e) { /* Intensionally left blank */ }
   }
   
    public static<A,B> Maybe<B> lookup(List<Pair<A,B>> list, A key) {
        final Maybe.Builder<B> result = new Maybe.Builder<B>();
        list.forEach(pair -> {
            if(pair.first.equals(key)) result.accept(pair.second);
        });
        return result.getMaybe();
    }

    public static<A,B,C> Maybe<Pair<B,C>> lookupPair(List<Triple<A,B,C>> list, A key) {
        final Maybe.Builder<Pair<B,C>> result = new Maybe.Builder<Pair<B,C>>();
        list.forEach(triple -> {
            if(triple.first.equals(key)) result.accept(new Pair(triple.second, triple.third));
        });
        return result.getMaybe();
    }

    public static<A,B,C> List<Triple<A,B,C>> replaceC(List<Triple<A,B,C>> list, B key, C replacement){
       return list.map(new Function<Triple<A,B,C>, Triple<A,B,C>>() {
           @Override
           public Triple<A, B, C> apply(Triple<A, B, C> triple) {
               if (triple.second.equals(key)) {
                   return new Triple<>(triple.first, triple.second, replacement);
               }
               return triple;
           }
       });
    }

   
    public static<A,E extends Exception> Stored<A> updateSingle(Stored<A> stored, Storage<A,E> storage, Function<Stored<A>,A> update) throws E, DeletedException {
        boolean updated = true;
        while(true) {
            try {
                return storage.update(stored,update.apply(stored));
            } catch (UpdatedException e) {
                stored = (Stored<A>)e.newObject;
            }
        }
    }
    
    public static<A,E extends Exception> void deleteSingle(Stored<A> stored, Storage<A,E> storage) throws E {
        while(true) {
            try {
                storage.delete(stored);
            } catch (UpdatedException e) {
                stored = (Stored<A>)e.newObject;
            } catch (DeletedException e) {
                return;
            }
        }
    }
}

