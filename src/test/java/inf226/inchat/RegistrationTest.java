package inf226.inchat;

import inf226.storage.ShittyPasswordException;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class RegistrationTest {

    private InChat createInChatInstance(){
        try {
            UUID testID = UUID.randomUUID();
            final String path = "test" + testID + ".db";
            final String dburl = "jdbc:sqlite:" + path;
            final Connection connection = DriverManager.getConnection(dburl);
            connection.createStatement().executeUpdate("PRAGMA foreign_keys = ON");
            UserStorage userStore = new UserStorage(connection);
            ChannelStorage channelStore = new ChannelStorage(connection);
            AccountStorage accountStore = new AccountStorage(connection, userStore, channelStore);
            SessionStorage sessionStore = new SessionStorage(connection, accountStore);
            connection.setAutoCommit(false);
            return new InChat(userStore, channelStore, accountStore, sessionStore, connection);
        }catch (SQLException e){
            return null; //Gud hjelpe oss hvis denne feiler
        }
    }

    @Test
    void passwordMustHave8CharsOrMore(){
        try{
            Password.create("bruh");
            fail();
        }catch(ShittyPasswordException ex){
            //Pass
        }
        try{
            Password.create("King Nebuchadnezzar II");
            //Pass
        }catch(ShittyPasswordException ex){
            fail();
        }
    }

    @Test
    void passwordCheckedWithCorrectInput(){
        String password = "King Nebuchadnezzar II";
        Password pass = Password.create(password);

        assertTrue(pass.check(password));
    }

    @Test
    void passwordCheckedWithWrongInput(){
        Password pass = Password.create("King Nebuchadnezzar II");

        assertFalse(pass.check("password"));
    }

    @Test
    void cannotRegisterTwiceWithTheSameUsername(){
        InChat inchat = createInChatInstance();
        assertFalse(inchat.register("Steinar", "King Nebuchadnezzar II").isNothing());
        assertTrue(inchat.register("Steinar", "King Nebuchadnezzar II").isNothing());
    }
}
