# INCHAT – The INsecure CHAT application

## Det vi har gjort

### 0
#### Del A og B - Brukeroppretting og passordsjekking

Vi har laget klassene UserName og Password.
Begge har regler for hva som er gyldig og ikke, for eksempel grenser for antall karakterer.
De kaster begge varianter av UserCreationException om inputtet ikke er i henhold.

Password-klassen bruker SCrypt til å behandle passordsjekking.
Når et nytt passord blir opprettet blir et tilfeldig salt generert, og passordet blir hashet med SCrypt og det saltet.
Når vi skal sjekke om en gitt streng tilfeldigvis er riktig passord hasher vi den strengen med samme saltet, og sammenligner.

Vi har endret litt på Account-tabellen. Istedet for å lagre selve passordet lagres en strengrepresentasjon av hashen, 
ved siden av en strengreresentasjon av saltet. Når passordet skal leses fra databasen leses de to strengene some bytearrays
og blir til Password igjen. Merk at det egentlige passordet til brukeren aldri blir lagret noen steder, istedet blir det glemt
etter Password-kostruktøren er ferdig, og hashen og saltet blir lagret istedet.

#### Del C

Vi la til noen smarte flagg på cookiene i Handler-klassen.

### 1 - SQL Injections

Vi har håndtert SQL-problemer ganske enkelt: vi har bare erstattet alle spørringer med PreparedStatement-mønsteret.

### 2 - XSS (Cross-Site Scripting)
Problemet med InChat var at når man registrerte ny bruker, laget en ny channel
eller skrev en melding på en channel var det mulighet for XSS angrep. Det XSS angrep gjør er å sende brukere til
uønskede sider. Så er noe likt med CSRF. Bare at ved et XSS angrep vil coden ikke hente ut info slik som i CSRF, men
man vil kunne utgi seg som personen som blir angrepet.
Så for å hindre dette lar vi ikke input bli tolket som HTML kode og eller queries med "?".

### 3 - CSRF (Cross-Site Request Forgery)
UUID - Universally Unique identifier.
Cookie - Lar nettsider huske deg, dine logg inns og hva du eventuelt har gjort på forskjellige sider.
Problemet CSRF er at man kan legge til ulik HTML kode i melding-boksen eller når man lager en ny channel som kan sende
brukere til andre nettsider. Bare ved et trykk på en link man ikke vet hva er eller dersom man trykker på navnet
til channel-en så kan man ende opp med å gi ut masse informasjon til brukeren som lagde channelen. Det er flere ting
som kan skje med personen som blir angrepet.
Måten man klarer å forhindre dette er å sjekke at cookie som nettstedet har og UUID-en som blir laget av programmet
er den samme. Altså man må sjekke at to forskjellige verdier, som dermed MÅ samsvare, for at man skal få tilgang til
inchat og eventuell info som inchat inneholder. Dette hindrer at andre ikke klarer å komme til info eller lure deg med
onde nettsider.
Så hver gang vi lager en channel vil en unik UUID bli laget å sjekket med InChat om den har den samme verdien i sin
cookie som det heter.
Man trenger ikke slike verdier nå man skal logge inn eller registrere en bruker siden man kan ikke sjekke en UUID mot
en cookie når ingen av dem er blitt opprettet. Så lenge man ikke er logget in er det ikke et behov for en cookie eller
en UUID, og derdem trenger man ikke å sjekke dem heller.
Så CSRF gjør at man gjør en uønsket handling på en side man er logget inn på bare ved å trykke på en link man ikke
vet hva er.



### 4 - Access Control

Vi har implementert access control ved å lagre en rolle for hver bruker i hver kanal.
Den rollen blir sjekket hver gang brukeren prøver å gjøre noe som hen kanskje/kanskje ikke har lov til.

Først og fremst er det gjort med en enum for hver av de fem rollene. 
Vi har oppdatert Account-klassen og AccountChannel til å også lagre denne enumen ved siden av hvert oppslag.
Alle metodene i InChat relatert til at brukeren skal gjøre noe sjekker nå at brukeren har lov til å gjøre det,
og kaster en exception hvis ikke. 

En mangel i programmet nå er at mange av tingene som brukeren ikke har lov til er tilsynelatende fortsatt tilgjengelig.
For eksempel får en Participant fortsatt et vindu der hen kan skrive inn meldinger, selv om de ikke kan sendes.
Når hen trykker send kommer hen til 404 Not Found. Så vi har implementert selve access control-delen av access control,
men vi har ikke gjort alle de visuelle oppdateringene som vi kanskje burde ha gjort.

### ω - Andre problemer

#### Fjerning av debug-kanalen og den hardkodede admin-brukeren
Det er ingen grunn til å ha direkte tilgang til databasen i produksjonskode.
Om det er nødvendig med direkte tilgang har vi det et annet sted som ikke er tilgengelig for brukerne.
Derfor fjernet vi både admin-brukeren og kanalen hans.

